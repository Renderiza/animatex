=begin

(c) RENDERIZA 2013

THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE.

== Information
Author:: 
	*Renderiza 

Name:: Animatex
Version:: 1.0.6
SU Version:: v8
Date:: 5/24/2013
Description:: Animatex allows you to animate textures with a specified x,y,z speeds.
Usage:: Add faces you want to animate to Animatex Layes and set x,y,z to animate when clicking play.
History::
1.0.0 Beta:: 
	* Original release.  
1.0.1 Beta::
	* Set FPS
1.0.2 Beta:: 5/15/2013
	* Animate multiple faces
	* 3 Layers of controls for x,y,z speeds
1.0.3 Beta:: 5/15/2013
	* Animatex works now while transitioning between scene camera animation. 
1.0.4 Beta:: 5/16/2013
	* Animatex works with groups now. 
1.0.5 Beta:: 5/21/2013
	* Plugin made into extention.
	* Fixed Buttons 
1.0.6 Beta:: 5/24/2013
	* Made compatible with Extension Warehouse
=end

module RND_Extensions

	module RND_Animatex
		require 'sketchup.rb'
		require 'extensions.rb'

		unless file_loaded?( __FILE__ )
			file_loaded( __FILE__ )
			# Create the extension. 
			ext = SketchupExtension.new 'Animatex', 'RND_Animatex/RND_Animatex_menus.rb'
			
			# Attach some nice info.
			ext.creator     = 'Renderiza'
			ext.version     = '1.0.6'
			ext.copyright   = '2013, Renderiza Studio.'
			ext.description = 'Animatex allows you to animate multiple textured faces by assigning them to any of the three Animatex layers which have their own x,y,z parameters to set speed.'

			# Register and load the extension on startup.
			Sketchup.register_extension ext, true
		end ;
		
	end ;
	
end ;

#file_loaded( __FILE__ )