=begin

(c) RENDERIZA 2013

THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE.

== Information
Author:: 
	*Renderiza 

Name:: Animatex
Version:: 1.0.6
SU Version:: v8
Date:: 5/24/2013
Description:: Animatex allows you to animate textures with a specified x,y,z speeds.
Usage:: Add faces you want to animate to Animatex Layes and set x,y,z to animate when clicking play.
History::
1.0.0  
	* Original release.  
1.0.1 
	* Set FPS
1.0.2 :: 5/15/2013
	* Animate multiple faces
	* 3 Layers of controls for x,y,z speeds
1.0.3 :: 5/15/2013
	* Animatex works now while transitioning between scene camera animation. 
1.0.4 :: 5/16/2013
	* Animatex works with groups now. 
1.0.5 :: 5/21/2013
	* Plugin made into extention.
	* Fixed Buttons 
1.0.6 :: 5/24/2013
	* Made compatible with Extension Warehouse
=end

module RND_Extensions

	module RND_Animatex

		class Animatex
		
			@@model = Sketchup.active_model ;
			@@path = File.dirname(__FILE__) ; # this plugin's folder
			@@rnd_animatex = "rnd_animatex.html" ;
			@@animatex_files = File.join(@@path, '/AnimatexFiles/') ;
			@@animatex_dlg = nil ;

			
			def initialize()
			
				@animatex_1_file =  File.join(@@path, @@rnd_animatex) ;
		   
				if (!@animatex_1_file) then 
					UI.messagebox("Cannot find file '#{@@rnd_animatex} in folder '#{@@path}'") ; 
					return ;
				end ; 
				
				if @@animatex_dlg == nil
					@@animatex_dlg = UI::WebDialog.new("Animatex v.1.0.6", false, "Animatex v.1.0.6", 315, 570, 70, 95, true) ;

					@@animatex_dlg.add_action_callback("push_frame") do |d,p| 
					push_frame(d,p) ; 
					end ;
				end ;
				
				@@animatex_dlg.set_size(315, 570) ;
				@@animatex_dlg.set_file(@animatex_1_file) ;
				@@animatex_dlg.show() ; 
				
				@animate= false ;
				@play1 = false ;
				@play2 = false ;
				@play3 = false ;
				@fixed = false ;
				
				@speed_x = 0 unless @speed_x ;
				@speed_y = 0 unless @speed_y ;
				@speed_z = 0 unless @speed_z ;
				@speed_2x = 0 unless @speed_2x ;
				@speed_2y = 0 unless @speed_2y ;
				@speed_2z = 0 unless @speed_2z ;
				@speed_3x = 0 unless @speed_3x ;
				@speed_3y = 0 unless @speed_3y ;
				@speed_3z = 0 unless @speed_3z ;
					
				Sketchup.send_action('selectSelectionTool:') ; #Selection Tool
			end ; #def 
			
			
			
			def push_frame(dialog,data)
					
				params = query_to_hash(data) ;  # parse state information 
				model = Sketchup.active_model ;
				sel = model.selection ;
				ents = model.active_entities ;	
				faces = ents.grep(Sketchup::Face)
				groups = ents.grep(Sketchup::Group)
				for group in groups
				  faces << group.entities.grep(Sketchup::Face)
				end
				faces.flatten! ### combine []'s
				faces.compact! ### remove 'nils'
				faces.uniq! ### to remove 'duplicates' if the groups have multiple copies
				layers = model.layers ;
				pages = model.pages
				num = pages.count
				
				if @animate
					
					if num != 0 && @fixed
						
						page = pages.selected_page.name ;	
						page_num = 0 ;
					
						pages.each do |e|
						  if e.name == page
							pages.selected_page = model.pages[page_num]
							break
						  end ;
						  page_num+=1
						end ;
					end	;
					
					animatex_1 = [] ;
					animatex_2 = [] ;
					animatex_3 = [] ;
					
					@speed_x += params['getx'].to_f ; 
					@speed_y += params['gety'].to_f ;
					@speed_z += params['getz'].to_f ;
					
					@speed_2x += params['get2x'].to_f ; 
					@speed_2y += params['get2y'].to_f ;
					@speed_2z += params['get2z'].to_f ;
					
					@speed_3x += params['get3x'].to_f ;
					@speed_3y += params['get3y'].to_f ;
					@speed_3z += params['get3z'].to_f ;
					
					faces.each do |e|
						if e.layer.name == 'animatex_1'
							animatex_1 << e ;
						end ;
						
						if e.layer.name == 'animatex_2'
							animatex_2 << e ;
						end ;
						
						if e.layer.name == 'animatex_3'
							animatex_3 << e ;
						end ;
					end ;
					
					if @play1 
						animatex_1.each do |e|
							next unless mat = e.material ;	
							mat = e.material ;	
							return nil if @speed_x == 0 && @speed_y==0 && @speed_z==0 && @speed_2x == 0 && @speed_2y==0 && @speed_2z==0 && @speed_3x == 0 && @speed_3y==0 && @speed_3z==0 ;### because then pts_array[0]==pts_array[1]
							pt_array = [Geom::Point3d.new(@speed_x, @speed_y, @speed_z), Geom::Point3d.new(0, 0, 0)] ;
							on_front = true	;
							e.position_material mat, pt_array, on_front ;
						end	;
					end ;
				
					if @play2 
						animatex_2.each do |e|
							next unless mat2 = e.material ;
							mat2 = e.material ;
							return nil if @speed_x == 0 && @speed_y==0 && @speed_z==0 && @speed_2x == 0 && @speed_2y==0 && @speed_2z==0 && @speed_3x == 0 && @speed_3y==0 && @speed_3z==0 ;### because then pts_array[0]==pts_array[1]
							pt2_array = [Geom::Point3d.new(@speed_2x, @speed_2y, @speed_2z), Geom::Point3d.new(0, 0, 0)] ;
							on2_front = true ;	
							e.position_material mat2, pt2_array, on2_front ;
						end ;						  
					end ; 
					
					if @play3
						animatex_3.each do |e|
							next unless mat3 = e.material ;
							mat3 = e.material ;
							return nil if @speed_x == 0 && @speed_y==0 && @speed_z==0 && @speed_2x == 0 && @speed_2y==0 && @speed_2z==0 && @speed_3x == 0 && @speed_3y==0 && @speed_3z==0 ;### because then pts_array[0]==pts_array[1]
							pt3_array = [Geom::Point3d.new(@speed_3x, @speed_3y, @speed_3z), Geom::Point3d.new(0, 0, 0)] ;
							on3_front = true ;	
							e.position_material mat3, pt3_array, on3_front ;
						end ;						  
					end ; 
				
				end ;
				
				#Fix Scene Lag
				if params['fixed'].to_s == "true" 
					@fixed = true ;
				else
					@fixed = false ;
				end
				
				#Add to Layers Buttons
				if params['add1'].to_s == "1" 
					new_animatex_1 = layers.add "animatex_1" ;	
					sel.each { |entity| entity.layer = new_animatex_1 } ;
					adda = 0 ;
					script = "top.add1 = " + adda.to_s + ";" ;
					dialog.execute_script(script) ;	
					Sketchup.send_action('selectSelectionTool:') ; #Selection Tool	
				end ;
				if params['add2'].to_s == "1" 
					new_animatex_2 = layers.add "animatex_2" ;
					sel.each { |entity| entity.layer = new_animatex_2 } ;
					addb = 0 ;
					script = "top.add2 = " + addb.to_s + ";" ;
					dialog.execute_script(script) ;	
					Sketchup.send_action('selectSelectionTool:') ; #Selection Tool	
				end	;
				if params['add3'].to_s == "1" 
					new_animatex_3 = layers.add "animatex_3" ;
					sel.each { |entity| entity.layer = new_animatex_3 } ;		
					addc = 0 ;
					script = "top.add3 = " + addc.to_s + ";" ;
					dialog.execute_script(script) ;	
					Sketchup.send_action('selectSelectionTool:') ; #Selection Tool	
				end ;
				
				#Remove from Layers
				if params['notadd1'].to_s == "1" 
					layer0 = layers.add "Layer0" ;
					sel.each { |entity| entity.layer = layer0 } ;
					notadda = 0 ;
					script = "top.notadd1 = " + notadda.to_s + ";" ;
					dialog.execute_script(script) ;	
					Sketchup.send_action('selectSelectionTool:') ; #Selection Tool	
				end ;
				if params['notadd2'].to_s == "1" 
					layer0 = layers.add "Layer0" ;
					sel.each { |entity| entity.layer = layer0 } ;
					notaddb = 0 ;
					script = "top.notadd2 = " + notaddb.to_s + ";" ;
					dialog.execute_script(script) ;	
					Sketchup.send_action('selectSelectionTool:') ; #Selection Tool	
				end ;
				if params['notadd3'].to_s == "1" 
					layer0 = layers.add "Layer0" ;
					sel.each { |entity| entity.layer = layer0 } ;
					notaddc = 0 ;
					script = "top.notadd3 = " + notaddc.to_s + ";" ;
					dialog.execute_script(script) ;	
					Sketchup.send_action('selectSelectionTool:') ; #Selection Tool	
				end ;
				
				#Play Buttons
				if params['play1'].to_s == "1" 
					new_animatex_1 = layers.add "animatex_1" ;
					@speed_x = params['getx'].to_f ;
					@speed_y = params['gety'].to_f ;
					@speed_z = params['getz'].to_f ;
					@animate = true ;
					@play1 = true ;
					playa = 0 ;
					script = "top.play1 = " + playa.to_s + ";" ;
					dialog.execute_script(script) ;	
					Sketchup.send_action('selectSelectionTool:') ; #Selection Tool	
				end ;
				if params['play2'].to_s == "1" 
					new_animatex_2 = layers.add "animatex_2" ;
					@animate = true ;
					@play2 = true ;
					@speed_2x = params['get2x'].to_f ; 
					@speed_2y = params['get2y'].to_f ;
					@speed_2z = params['get2z'].to_f ;
					playb = 0 ;
					script = "top.play2 = " + playb.to_s + ";" ;
					dialog.execute_script(script) ;	
					Sketchup.send_action('selectSelectionTool:') ; #Selection Tool	
				end ;	
				if params['play3'].to_s == "1" 
					new_animatex_3 = layers.add "animatex_3" ;
					@animate = true ;
					@play3 = true ;
					@speed_3x = params['get3x'].to_f ; 
					@speed_3y = params['get3y'].to_f ;
					@speed_3z = params['get3z'].to_f ;
					playc = 0 ;
					script = "top.play3 = " + playc.to_s + ";" ;
					dialog.execute_script(script) ;	
					Sketchup.send_action('selectSelectionTool:') ; #Selection Tool	
				end ;
				
				#Stop Buttons
				if params['stop1'].to_s == "1" 		
					@play1 = false ;	
					stopa = 0 ;
					script = "top.stop1 = " + stopa.to_s + ";" ;
					dialog.execute_script(script) ;	
					Sketchup.send_action('selectSelectionTool:') ; #Selection Tool	
				end ;
				if params['stop2'].to_s == "1" 	
					@play2 = false ;
					stopb = 0 ;
					script = "top.stop2 = " + stopb.to_s + ";" ;
					dialog.execute_script(script) ;	
					Sketchup.send_action('selectSelectionTool:') ; #Selection Tool	
				end ;
				if params['stop3'].to_s == "1" 
					@play3 = false ;
					stopc = 0 ;
					script = "top.stop3 = " + stopc.to_s + ";" ;
					dialog.execute_script(script) ;	
					Sketchup.send_action('selectSelectionTool:') ; #Selection Tool	
				end ;
				
				#Click Animate
				if params['active'].to_s == "1" 
					new_animatex_1 = layers.add "animatex_1" ;
					new_animatex_2 = layers.add "animatex_2" ;
					new_animatex_3 = layers.add "animatex_3" ;
					@speed_x = params['getx'].to_f ;
					@speed_y = params['gety'].to_f ;
					@speed_z = params['getz'].to_f ;
					@speed_2x = params['get2x'].to_f ; 
					@speed_2y = params['get2y'].to_f ;
					@speed_2z = params['get2z'].to_f ;
					@speed_3x = params['get3x'].to_f ;
					@speed_3y = params['get3y'].to_f ;
					@speed_3z = params['get3z'].to_f ;
					@animate = true ;
					@play1 = true ;
					@play2 = true ;
					@play3 = true ;
					activate = 0 ;
					script = "top.active = " + activate.to_s + ";" ;
					dialog.execute_script(script) ;	
					Sketchup.send_action('selectSelectionTool:') ; #Selection Tool	
				end ;
				
				#Click Pause
				if params['pause'].to_s == "1" 			
					@animate = false ;
					@play1 = false ;
					@play2 = false ;
					@play3 = false ;
					paused = 0 ;
					script = "top.pause = " + paused.to_s + ";" ;
					dialog.execute_script(script) ;
					Sketchup.send_action('selectSelectionTool:') ; #Selection Tool
				end ;
			
				#Click Done
				if params['done'].to_s == "1" 
					@animate= false ;
					@play1 = false ;
					@play2 = false ;
					@play3 = false ;
					Sketchup.send_action('selectSelectionTool:') ; #Selection Tool
					@@animatex_dlg.close ;	
				end ;
						
			end ;#def
			
			
			
			def unescape(string)
				if string != nil
					string = string.gsub(/\+/, ' ').gsub(/((?:%[0-9a-fA-F]{2})+)/n) do
						[$1.delete('%')].pack('H*') ;
					end ;
				end ;
				return string
			end ;#def
			
			
			
			def query_to_hash(query)
				param_pairs = query.split('&') ;
				param_hash = {} ;
				for param in param_pairs
					name, value = param.split('=') ;
					name = unescape(name) ;
					value = unescape(value) ;
					param_hash[name] = value ;
				end ;
				return param_hash
			end ;#def
			

			
		end ;#class
		
	end ;#module

end ;#module

file_loaded( File.basename(__FILE__) )   # Mark the script loaded. 