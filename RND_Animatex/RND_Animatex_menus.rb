=begin

(c) RENDERIZA 2013

THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE.

== Information
Author:: 
	*Renderiza 

Name:: Animatex
Version:: 1.0.6
SU Version:: v8
Date:: 5/24/2013
Description:: Animatex allows you to animate textures with a specified x,y,z speeds.
Usage:: Add faces you want to animate to Animatex Layes and set x,y,z to animate when clicking play.
History::
1.0.0 Beta:: 
	* Original release.  
1.0.1 Beta::
	* Set FPS
1.0.2 Beta:: 5/15/2013
	* Animate multiple faces
	* 3 Layers of controls for x,y,z speeds
1.0.3 Beta:: 5/15/2013
	* Animatex works now while transitioning between scene camera animation. 
1.0.4 Beta:: 5/16/2013
	* Animatex works with groups now. 
1.0.5 Beta:: 5/21/2013
	* Plugin made into extention.
	* Fixed Buttons 
1.0.6 Beta:: 5/24/2013
	* Made compatible with Extension Warehouse
=end

module RND_Extensions

	module RND_Animatex
		require 'sketchup.rb'
		require 'RND_Animatex/RND_Animatex_loader.rb'
	end ;

	if !file_loaded?('rnd_menu_loader')
		@@rnd_tools_menu = UI.menu("Plugins").add_submenu("Renderiza Tools") ;
	end ;
	
	#------New menu Items---------------------------
	if !file_loaded?('rnd_ew_loader')
		@@rnd_ew_menu = @@rnd_tools_menu.add_submenu("Websites")
		@@rnd_ew_menu.add_separator
		@@rnd_ew_menu.add_item("PluginStore"){UI.openURL('http://sketchucation.com/resources/pluginstore?pauthor=renderiza')}
		@@rnd_ew_menu.add_item("Extension Warehouse"){UI.openURL('http://extensions.sketchup.com/en/user/5451/store')}
		@@rnd_tools_menu.add_separator
		@@rnd_ew_menu.add_separator
	end ;
	#------------------------------------------------
	if !file_loaded?(__FILE__) then
		@@rnd_tools_menu.add_item('Animatex') { 
		Sketchup.active_model.select_tool RND_Extensions::RND_Animatex::Animatex.new ; 
		}
		# Add toolbar
		rnd_animatex_tb = UI::Toolbar.new "Animatex" ;
		rnd_animatex_cmd = UI::Command.new("Animatex") { 
		Sketchup.active_model.select_tool RND_Extensions::RND_Animatex::Animatex.new ; 
		} 
		rnd_animatex_cmd.small_icon = "img/rnd_animatex_1_16.png" ;
		rnd_animatex_cmd.large_icon = "img/rnd_animatex_1_24.png" ;
		rnd_animatex_cmd.tooltip = "Animatex" ;
		rnd_animatex_cmd.status_bar_text = " Animate Textures" ;
		rnd_animatex_cmd.menu_text = "Animatex" ;
		rnd_animatextb = rnd_animatex_tb.add_item rnd_animatex_cmd ;
		rnd_animatex_tb.show ;
	end ;
	
	file_loaded('rnd_ew_loader')    
	file_loaded('rnd_menu_loader')
	file_loaded(__FILE__)
end ;